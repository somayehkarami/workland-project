const redux = require("redux");

const counterReducer = (state = { counter: 5 }, action) => {
  //reducer function with two prameters old State + dispatched action and return a new state objet

  if (action.type === "increment") {
    return {
      counter: state.counter + 1,
    };
  }

  if (action.type === "decrement") {
    return {
      counter: state.counter - 1,
    };
  }
  return state;
};

const store = redux.createStore(counterReducer);
console.log(store.getState());

//needs someone who subscribes to that store
const counterSubscriber = () => {
  const latestState = store.getState(); //give us the latest snapshot after it was updated.
  console.log(latestState);
};

store.subscribe(counterSubscriber);

store.dispatch({ type: "increment" }); //its a method that dispatch an action.
store.dispatch({ type: "decrement" });

// and we need an action that can be dispatch.
